package com.ictcampus.lab.base.control.model;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
public class BookResponse {
	private Long id;
	private String title;
	private String author;
	private String isbn; // Changed from ISDB to ISBN
	private String description; // Changed to lowercase

	public BookResponse(Long id, String title, String author, String isbn, String description) {
		this.id = id;
		this.title = title;
		this.author = author;
		this.isbn = isbn; // Changed from ISDB to ISBN
		this.description = description; // Changed to lowercase
	}

	// Getters and setters
	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(final String author) {
		this.author = author;
	}

	public String getIsbn() {
		return this.isbn;
	}

	public void setIsbn(final String isbn) { // Changed from setISDB to setIsbn
		this.isbn = isbn;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public class BookUpdateRequest {

		private String title;
		private String author;
		private String isbn;
		private String description;

		// Getters and Setters
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getAuthor() { return author; }

		public void setAuthor(String author) {}

		public String getIsbn() { return isbn; }

		public void setIsbn(String isbn) {}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
	}

}
