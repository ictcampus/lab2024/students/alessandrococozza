package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.WorldRequest;
import com.ictcampus.lab.base.control.model.WorldResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/worlds" )
@AllArgsConstructor
@Slf4j
public class WorldController {
	private List<WorldResponse> list = new ArrayList<>();


	@GetMapping(value = "/init", produces = { MediaType.APPLICATION_JSON_VALUE} )
	public String init() {
		list = generateworld();

		return "Init OK";

	}

	@GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<WorldResponse> getWorlds() {

		return list;
	}

	private List<WorldResponse> generateworld() {
		List<WorldResponse>list = new ArrayList<>();

		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( 1L );
		worldResponse.setName( "Terra" );
		worldResponse.setSystem( "Sistema Solare" );

		list.add( worldResponse );

		WorldResponse worldResponse2 = new WorldResponse();
		worldResponse2.setId( 2L );
		worldResponse2.setName( "Marte" );
		worldResponse2.setSystem( "Sistema Solare" );

		list.add( worldResponse2 );

		WorldResponse worldResponse3 = new WorldResponse();
		worldResponse3.setId( 3L );
		worldResponse3.setName( "Venere" );
		worldResponse3.setSystem( "Sistema Solare" );

		list.add( worldResponse3 );

		WorldResponse worldResponse4 = new WorldResponse();
		worldResponse4.setId( 4L );
		worldResponse4.setName( "Saturno" );
		worldResponse4.setSystem( "Sistema Solare" );

		list.add( worldResponse4 );

		return list;



	}


	@GetMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public WorldResponse getWorld(
			@PathVariable(name = "id") Long id
	) {
		/*return list.get( id.intValue() - 1 );*/

		for ( WorldResponse planet : list ) {
			if ( planet.getId().equals( id ) ) {
				return planet;
			}
		}
		return null;
		/*WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( 1L );
		worldResponse.setName( "Terra" );
		worldResponse.setSystem( "Sistema Solare" );

		WorldResponse worldResponse2 = new WorldResponse();
		worldResponse2.setId( 2L );
		worldResponse2.setName( "Marte" );
		worldResponse2.setSystem( "Sistema Solare" );

		WorldResponse worldResponse3 = new WorldResponse();
		worldResponse3.setId( 3L );
		worldResponse3.setName( "Venere" );
		worldResponse3.setSystem( "Sistema Solare" );

		WorldResponse worldResponse4 = new WorldResponse();
		worldResponse4.setId( 4L );
		worldResponse4.setName( "Saturno" );
		worldResponse4.setSystem( "Sistema Solare" );

		return worldResponse;*/


	}

	@PostMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public Long createWorld(
			@RequestBody WorldRequest worldRequest
	) {
		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( 1L );
		worldResponse.setName( worldRequest.getName() );
		worldResponse.setSystem( worldRequest.getSystem() );

		return worldResponse.getId();
	}

	@PutMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void editWorld(
			@PathVariable(name = "id") Long id,
			@RequestBody WorldRequest worldRequest
	) {
		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( id );
		worldResponse.setName( worldRequest.getName() );
		worldResponse.setSystem( worldRequest.getSystem() );
	}

	@DeleteMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void deleteWorld(
			@PathVariable(name = "id") Long id
	) {
		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( id );
	}
}



