package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.BookResponse;
import com.ictcampus.lab.base.control.model.BookRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("/api/v1/books")
@AllArgsConstructor
@Slf4j
public class BookController {
	private List<BookResponse> list = new ArrayList<>();

	public BookController() {
		this.list = generateBooks();
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<BookResponse> getBooks() {
		log.info("Returning list of books: {}", list);
		return this.list;
	}

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public BookResponse getBook(@PathVariable(name = "id") Long id) throws ChangeSetPersister.NotFoundException {
		BookResponse bookResponse = this.findBookById(id);
		if (bookResponse == null) {
			log.info("Book with ID [{}] not found", id);
			throw new ChangeSetPersister.NotFoundException();
		}

		log.info("Returning book with ID [{}]: {}", id, bookResponse);
		return bookResponse;
	}
	@GetMapping(value = "/title/{title}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<BookResponse> searchByTitle(@PathVariable(name = "title") String title) {
		List<BookResponse> results = new ArrayList<>();
		for (BookResponse book : list) {
			if (book.getTitle().toLowerCase().contains(title.toLowerCase())) {
				results.add(book);
			}
		}
		return results;
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public Long createBook(@RequestBody BookRequest bookRequest) {
		log.info("Creating new book with data [{}]", bookRequest);
		Long id = this.lastId();
		log.info("New book ID: [{}]", id);

		String title = bookRequest.getTitle() != null ? bookRequest.getTitle() : "Unknown Title";
		String author = bookRequest.getAuthor() != null ? bookRequest.getAuthor() : "Unknown Author";
		String isbn = bookRequest.getIsbn() != null ? bookRequest.getIsbn() : "Unknown ISBN";
		String description = bookRequest.getDescription() != null ? bookRequest.getDescription() : "No Description";

		BookResponse bookResponse = new BookResponse(id, title, author, isbn, description);
		this.list.add(bookResponse);

		return id;
	}

	@PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void editBook(@PathVariable(name = "id") Long id, @RequestBody BookRequest bookRequest) {
		log.info("Updating book with ID [{}] and data [{}]", id, bookRequest);
		this.updateById(id, bookRequest);
	}

	@DeleteMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void deleteBook(@PathVariable(name = "id") Long id) {
		log.info("Deleting book with ID [{}]", id);
		this.deleteById(id);
	}

	@GetMapping(value = "/shuffle", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<BookResponse> shuffleBooks() {
		log.info("Shuffling the list of books.");
		Collections.shuffle(this.list);
		log.info("Shuffled list of books: {}", list);
		return this.list;
	}

	private List<BookResponse> generateBooks() {
		Long index = 1L;
		return new ArrayList<>(Arrays.asList(

				new BookResponse(index++, "1", "1", "w34rfrsgf", "dgh"),
				new BookResponse(index++, "2", "2", "432rfewf", "Descrdghfiption5"),
				new BookResponse(index++, "3", "3", "543rw", "dfgfdg"),
				new BookResponse(index++, "4", "4", "wert4235", "dfg"),
				new BookResponse(index++, "5", "5", "wer32w", "dfgsd")
		));
	}

	private BookResponse findBookById(Long id) {
		return this.list.stream()
				.filter(book -> book.getId().equals(id))
				.findFirst()
				.orElse(null);
	}


	private Long lastId() {
		return this.list.stream()
				.mapToLong(BookResponse::getId)
				.max()
				.orElse(0L) + 1;
	}

	private void updateById(Long id, BookRequest newData) {
		this.list.stream()
				.filter(book -> book.getId().equals(id))
				.findFirst()
				.ifPresent(book -> {
					book.setTitle(newData.getTitle() != null ? newData.getTitle() : book.getTitle());
					book.setAuthor(newData.getAuthor() != null ? newData.getAuthor() : book.getAuthor());
					book.setIsbn(newData.getIsbn() != null ? newData.getIsbn() : book.getIsbn());
					book.setDescription(newData.getDescription() != null ? newData.getDescription() : book.getDescription());
				});
	}

	private void deleteById(Long id) {
		this.list.removeIf(book -> book.getId().equals(id));
	}
}
